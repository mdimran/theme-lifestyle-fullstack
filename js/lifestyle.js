/**
 * Created by Mohammad on 6/14/2017.
 */


jQuery(document).ready(function(){
    $(".show-icons").hide();
    $(".hide-icons").on("click", function(){
    	$(this).next().show();
    	$(this).hide();

    });
});



jQuery(document).ready(function($){
    var offset = 100;
    var speed = 600;
    var duration = 500;
    $(window).scroll(function(){
        if ($(this).scrollTop() < offset) {
            $('.topbutton') .fadeOut(duration);
        } else {
            $('.topbutton') .fadeIn(duration);
        }
    });
    $('.topbutton').on('click', function(){
        $('html, body').animate({scrollTop:0}, speed);
        return false;
    });
});

