<a  class="topbutton" href="">
    <i class="fa fa-chevron-up" aria-hidden="true"></i>
</a>
<div class="footer-style">
    <footer class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <?php dynamic_sidebar('footer_widgets-1') ?>
                </div>
                <div class="col-md-3">
                    <?php dynamic_sidebar('footer_widgets-2') ?>
                </div>
                <div class="col-md-3">
                    <?php dynamic_sidebar('footer_widgets-3') ?>
                </div>
                <div class="col-md-3">
                    <?php dynamic_sidebar('footer_widgets-4') ?>
                </div>
            </div>
        </div>
        <div class="site-info">
            <?php
            do_action('lifestyle_credits');
            ?>
            <hr class="hr-style" width="70%">
            <div class="footer-copyright">
                <p class="text-center"> © <?php the_time('Y'); ?> <b class="text-center text-uppercase"><a
                            href="<?php echo get_home_url(); ?>"><?php bloginfo('name') ?></a></b>. All right reserved.


                </p>

            </div> 
             
             


            <?php
            function my_social_icons_output() {

                $social_sites = ct_tribes_social_array();

                foreach ( $social_sites as $social_site => $profile ) {

                        if ( strlen( get_theme_mod( $social_site ) ) > 0 ) {
                            $active_sites[ $social_site ] = $social_site;
                        }
                    }

            if ( ! empty( $active_sites ) ) {

                echo '<ul class="social-media-icons">';
                foreach ( $active_sites as $key => $active_site ) { 
                    $class = 'fa fa-' . $active_site; ?>
                <li class="aaa">
                    <a class="<?php echo esc_attr( $active_site ); ?>" target="_blank" href="<?php echo esc_url( get_theme_mod( $key ) ); ?>">
                        <i class="<?php echo esc_attr( $class ); ?>" title="<?php echo esc_attr( $active_site ); ?>"></i>
                    </a>
                </li>&nbsp;&nbsp;
                <?php 
                } 
                echo "</ul>";

            }
}

            add_action('wp_footer', 'my_social_icons_output');
            ?>

            <?php wp_footer(); ?>
        </div><!-- .site-info -->
    </footer><!-- .site-footer -->
</div>
</body>
</html>









