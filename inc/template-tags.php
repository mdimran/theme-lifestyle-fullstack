<?php

//----------------------------------------------------------------------
//  Posts navigation link. <- NEW post  |   OLD Post ->
//----------------------------------------------------------------------

if ( ! function_exists( 'story_posts_navigation' ) ) :

	function story_posts_navigation() {
		if ( $GLOBALS[ 'wp_query' ]->max_num_pages > 1 ) {
			get_template_part( 'template-parts/posts', 'navigation' );
		}
	}
endif;

//----------------------------------------------------------------------
//  Blog Pagination
//----------------------------------------------------------------------

if ( ! function_exists( 'story_posts_pagination' ) ) :
	function story_posts_pagination() {

		if ( $GLOBALS[ 'wp_query' ]->max_num_pages > 1 ) {
			$big   = 999999999; // need an unlikely integer
			$items = paginate_links( apply_filters( 'story_posts_pagination_paginate_links', array(
					'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format'    => '?paged=%#%',
					'prev_next' => TRUE,
					'current'   => max( 1, get_query_var( 'paged' ) ),
					'total'     => $GLOBALS[ 'wp_query' ]->max_num_pages,
					'type'      => 'array',
					'prev_text' => ' <p>PREVIOUS</p>',
					'next_text' => '<p>NEXT</p> ',
					'end_size'  => 1,
					'mid_size'  => 1
			) ) );

			$pagination = "<div class=\"pagination-wrap clearfix\"><ul class=\"pagination navigation\"><li>";
			$pagination .= join( "</li><li>", (array) $items );
			$pagination .= "</li></ul></div>";

			echo apply_filters( 'story_posts_pagination', $pagination, $items, $GLOBALS[ 'wp_query' ] );
		}
	}
endif;

//----------------------------------------------------------------------
// Like option in Post
//----------------------------------------------------------------------


function getPostLikeLink($post_id)
{
	$themename = "lifestyle";

	$vote_count = get_post_meta($post_id, "votes_count", true);

	if (empty($vote_count)) {

		$vote_count = 0;

	}

	$output = '<p class="post-like">';
	if(!is_user_logged_in() || hasAlreadyVoted($post_id) )
		$output .= ' <span title="'.__('I like this article', $themename).'" class="like alreadyvoted"><i class="fa fa-heart heart1" aria-hidden="true"></i></span>&nbsp;';
	else
		$output .= '<a href="#" data-post_id="'.$post_id.'">
		<span  title="'.__('I like this article', $themename).'"class="like-padding like"><i class="fa fa-heart-o heart2" aria-hidden="true"></i></span>
	</a>&nbsp;';
	$output .= '<span class="count">'.$vote_count.'</span></p>';

	return $output;
}


?>