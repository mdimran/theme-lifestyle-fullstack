<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title> Life Style </title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="ls-header">
    <div class="ls-header-design">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 padding-zero">
                    <nav class=" navbar-default text-center">
                        <div class="navbar-header ">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'primary',
                                    'container' => 'false',
                                    'menu_class' => 'nav navbar-nav'
                                )
                            );
                            ?>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="ls-logo">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center text-uppercase logo-style"><a href = "<?php echo get_home_url(); ?>" ><?php bloginfo('name') ?></a></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
