<article id="post-<?php the_ID(); ?>"<?php post_class(); ?>>

    <div class="ls-content-design">
        <div class="row">
            <?php if (has_post_thumbnail()): ?>
                <div class="col-md-12">
                    <div class="img-responsive post-padding-bottom img-style"><?php the_post_thumbnail(); ?></div>
                    <header class="post-padding-bottom ">
                        <?php the_title(sprintf('<h1 class=" title-font"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>
                    </header>
                    <small class="cat-font"> <?php the_category(' '); ?></small>
                    <?php get_template_part('template-parts/post-meta'); ?>

                    <div class="post-padding-bottom text-justify content-font ">
                        <?php the_content(); ?>
                    </div>




                </div>
            <?php else: ?>
                <div class="col-md-12">
                    <header class="post-padding-bottom ">
                        <?php the_title(sprintf('<h1 class=" title-font"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>
                    </header>
                    <small class="cat-font"> <?php the_category(' '); ?></small>
                    <?php get_template_part('template-parts/post-meta'); ?>

                    <div class="post-padding-bottom text-justify content-font ">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</article>