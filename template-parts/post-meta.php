<ul class="list-inline post-entry-meta meta-padding post-padding-bottom">
    <li class="posted-on meta-style">
        <a href="<?php the_permalink() ?>">
            <i class="fa fa-clock-o"></i> <?php echo the_time('jS F, Y')?>
        </a>
    </li>

    <li class="posted-by meta-style user-img">
        <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 16); ?> <?php the_author(); ?></a>
    </li>

    <li class="post-comment meta-style">
        <i class="fa fa-comment-o"></i> <?php comments_number( 'no comments', 'one comment', '% comments' ); ?>.
    </li>
    <li>
    
    
    <li class="pull-right">
        <span class="hide-icons">
            <a>
                <i class="fa fa-mail-forward"></i>
            </a>
        </span>

        <span class="social-share show-icons">
            <span><a href=' http://www.facebook.com/sharer.php?url=" . <?php the_permalink(); ?> . " ' target='_blank' ><i class="fa fa-facebook"></i></a></span>
            <span><a href='https://twitter.com/share?url=" . <?php the_permalink(); ?> . " ' target='_blank' ><i class="fa fa-twitter"></i></a></span>
            <span><a href='https://plus.google.com/share?url=" . <?php the_permalink(); ?> . "' target='_blank'  ><i class="fa fa-google-plus"></i></a></span>
            <span><a href='http://www.tumblr.com/share/link?url=" . <?php the_permalink(); ?> . "' target='_blank'  ><i class="fa fa-tumblr"></i></a></span>

        </span>
    </li> 
    <li class="pull-right">
            <?php echo getPostLikeLink(get_the_ID());?>
    </li>
    </li>

    


</ul>