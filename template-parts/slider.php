<?php
add_action('content_before_posts', 'posts_slider_before_content');
function posts_slider_before_content()
{
    //Code for carousel
    ?>
    <!--            carousel-->
    <div id="lifestyle-carousel" class="carousel slide " data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $categories = get_categories();
            $count = 0;
            $indi = '';
            foreach ($categories as $category):
                $args = array(
                    'type' => 'post',
                    'posts_per_page' => 1,
                    'category__in' => $category->term_id,
                    'category__not_in' => array(),
                );
                $lastBlog = new wp_Query($args);
                if ($lastBlog->have_posts()) :
                    while ($lastBlog->have_posts()) : $lastBlog->the_post(); ?>
                        <div class="item <?php if ($count == 0):echo 'active'; endif ?>">
                            <div class="carousel-style">
                                <?php the_post_thumbnail('full'); ?>
                            </div>
                            <div class="carousel-caption">
                                <div class="cat-style"> <?php the_category(' & ') ?> </div>
                                <?php the_title(sprintf('<h1 class="enter-title"><a href="%s">', esc_url(get_permalink())), '</a></h1>'); ?>
                                <?php get_template_part('template-parts/post-meta-slider'); ?>
                            </div>
                        </div>
                        <?php $indi .= '<li data-target="#lifestyle-carousel" data-slide-to="' . $count . '"
                                class="'; ?>
                        <?php if ($count == 0):$indi .= 'active'; endif ?>
                        <?php $indi .= '"></li>' ?>
                        <?php
                        $count++;
                    endwhile;
                endif;
                wp_reset_postdata();

            endforeach;
            ?>
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php echo $indi; ?>
            </ol>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#lifestyle-carousel" role="button" data-slide="prev">
            <span class="fa-style-left fa fa-angle-left " aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#lifestyle-carousel" role="button" data-slide="next">
            <span class="fa-style-right fa fa-angle-right " aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--            carousel-->
    <?php
}
