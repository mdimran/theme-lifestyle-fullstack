<ul class="list-inline post-entry-meta meta-carousel-padding post-padding-bottom">
    <li class="posted-on">
        <a href="<?php the_permalink() ?>">
            <i class="fa fa-clock-o"></i> <?php echo the_time('jS F, Y')?>
        </a>
    </li>
    <li class="posted-by">
        <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) )); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 16); ?> <?php the_author(); ?></a>
    </li>

</ul>