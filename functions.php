<?php

require get_template_directory() . "/inc/helper.php";
require get_template_directory() . "/inc/template-tags.php";
require get_template_directory() . "/inc/widget/class-wp-widget-lifestyle-recent-posts.php";
require get_template_directory() . "/inc/widget/address-widget.php";
require get_template_directory() . "/inc/widget/popular-post-widget.php";
require get_template_directory() . "/lifestyle-social.php";
require get_template_directory() . "/template-parts/slider.php";



//include enqueue
function lifestyle_script_enqueue()
{

    wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.7', 'all');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0', 'all');
//    wp_enqueue_style('Lora', '<link href="https://fonts.googleapis.com/css?family=Lora:700" rel="stylesheet">',false);
//    wp_enqueue_style('Lora', '<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">',false);
    wp_enqueue_style('custom-style', get_template_directory_uri() . '/css/lifestyle.css', array(), '1.1.0', 'all');
    wp_enqueue_style('custom-style-1', get_template_directory_uri() . '/p-css/master-preset.css', array(), '1.1.0', 'all');


    wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true);
    wp_enqueue_script('jQuery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '3.2.1', true);
    wp_enqueue_script('custom-script', get_template_directory_uri() . '/js/lifestyle.js', array(), '1.0', true);


    wp_enqueue_script('like_post', get_template_directory_uri() . '/js/post-like.js', array('jquery'), '1.0', true);
    wp_localize_script('like_post', 'ajax_var', array(
        'url' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('ajax-nonce')
    ));

}

add_action('wp_enqueue_scripts', 'lifestyle_script_enqueue');


//custom menu
function lifestyle_custom_menu()
{
    add_theme_support('menus');
    register_nav_menu('primary', 'Header Nav');
    register_nav_menu('secondary', 'Footer Nav');
}

add_action('init', 'lifestyle_custom_menu');

//theme support
add_theme_support('html5', array('search-form'));
//add_image_size('ls-img-design', 750, 384, TRUE);
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
add_theme_support('post-formats', array(
    'aside',
    'image',
    'video',
    'quote',
    'link',
    'gallery',
    'audio',
));

//side-bar

function lifestyle_widgets_setup()
{
    register_sidebar(array(
        'name' => ('Blog Sidebar'),
        'id' => 'sidebar-1',
        'description' => ('Add widgets here to appear in your sidebar on blog posts and archive pages.'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => 'Footer Sidebar-1',
        'id' => 'footer_widgets-1',
        'description' => 'Place widgets for the footer here.',
        'before_widget' => '<div class="widget-title">',
        'after_widget' => '</div>'
    ));
    register_sidebar(array(
        'name' => 'Footer Sidebar-2',
        'id' => 'footer_widgets-2',
        'description' => 'Place widgets for the footer here.',
        'before_widget' => '<div class="widget-title">',
        'after_widget' => '</div>'
    ));
    register_sidebar(array(
        'name' => 'Footer Sidebar-3',
        'id' => 'footer_widgets-3',
        'description' => 'Place widgets for the footer here.',
        'before_widget' => '<div class="widget-title">',
        'after_widget' => '</div>'
    ));
    register_sidebar(array(
        'name' => 'Footer Sidebar-4',
        'id' => 'footer_widgets-4',
        'description' => 'Place widgets for the footer here.',
        'before_widget' => '<div class="widget-title">',
        'after_widget' => '</div>'
    ));

}

add_action('widgets_init', 'lifestyle_widgets_setup');

//for popular post
function wpb_set_post_views($postID)
{
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//To keep the count accurate, lets get rid of prefetching
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views($post_id)
{
    if (!is_single()) return;
    if (empty ($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}

add_action('wp_head', 'wpb_track_post_views');

add_action('wp_ajax_nopriv_post-like', 'post_like');
add_action('wp_ajax_post-like', 'post_like');


function post_like()
{

    // Check for nonce security
    $nonce = $_POST['nonce'];

    if (!wp_verify_nonce($nonce, 'ajax-nonce'))
        die ('Not Secured!');

    if (isset($_POST['post_like'])) {

        $post_id = $_POST['post_id'];
        $uesr_id = get_current_user_id();
        $meta_user_ID = $uesr_id;
        $meta_count = get_post_meta($post_id, "votes_count", true);

        if (!hasAlreadyVoted($post_id)) {
            //  $voted_IP[$ip] = time();

            // Save IP and increase votes count
            update_post_meta($post_id, "user_ID", $meta_user_ID);
            update_post_meta($post_id, "votes_count", ++$meta_count);

            // Display count (ie jQuery return value)

            die($meta_count);
            //print_r($meta_count);
        } else
            echo "already";
    }
    exit;
}


function hasAlreadyVoted($post_id)
{

// Retrieve post votes IPs
    $uesr_id = get_current_user_id();
    $current_user_id = $uesr_id;


    //$vote_count = get_post_meta($post_id, "votes_count");
    $meta_user_ID = get_post_meta($post_id, "user_ID");

    // If user has already voted
    if (in_array($current_user_id, $meta_user_ID)) {
        return true;
    } else {
        return false;
    }
}




























